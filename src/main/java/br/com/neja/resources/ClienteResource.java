package br.com.neja.resources;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.apache.commons.mail.EmailException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.neja.domain.Cliente;
import br.com.neja.dto.ClienteDTO;
import br.com.neja.dto.ClienteNewDTO;
import br.com.neja.services.ClienteService;

@RestController
@RequestMapping(value = "/clientes")
public class ClienteResource {
	
	
	@Autowired
	private ClienteService service;
	
	@Autowired
	private BCryptPasswordEncoder crypt;

	
	@RequestMapping(value="/{id}",method=RequestMethod.GET)
	public ResponseEntity<Cliente> find(@PathVariable  Integer id) {
		Cliente obj =  service.find(id);
		return ResponseEntity.ok().body(obj);
		
	}
	
	@RequestMapping(value="/email",method=RequestMethod.GET)
	public ResponseEntity<Cliente> find(@RequestParam(value="value") String email) {
		Cliente obj = service.findByEmailResource(email);
		return ResponseEntity.ok().body(obj);
	}
	
	@RequestMapping(value="/email_update",method=RequestMethod.GET)
	public ResponseEntity<Cliente> findByEmail(@RequestParam(value="value") String email,@RequestParam(value="senha") String senha) {
		Cliente obj = service.findByEmailResource(email);
		obj.setSenha(crypt.encode(senha));
		obj.setFirstAcess(false);
		obj = service.update(obj);
		return ResponseEntity.ok().body(obj);
	}
	
	
	@RequestMapping(value="/find_by_email/{email}",method=RequestMethod.GET)
	public ResponseEntity<Cliente> findEmail(@PathVariable String email) {
		Cliente obj = service.findByEmail(email);
		
		return ResponseEntity.ok().body(obj);
	}
	
	
	@RequestMapping(value="/find_by_cpf_cnpj/{cpf}",method=RequestMethod.GET)
	public ResponseEntity<Cliente> findCpfCnpj(@PathVariable String cpf) {
		Cliente obj = service.findByCpf(cpf);
		return ResponseEntity.ok().body(obj);
	}
		
	
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<ClienteDTO>> findAll() {
		List<Cliente> list = service.findAll();
		List<ClienteDTO> listDTO = list.stream().map(obj -> new ClienteDTO(obj)).collect(Collectors.toList());
		return ResponseEntity.ok().body(listDTO);
	}
	
	
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(value = "/page", method = RequestMethod.GET)
	public ResponseEntity<Page<ClienteDTO>> findPage(
			// parametros
			@RequestParam(defaultValue = "0", value = "page") Integer page,
			@RequestParam(defaultValue = "24", value = "linesPerPage") Integer linesPerPage,
			@RequestParam(defaultValue = "nome", value = "orderBy") String orderBy,
			@RequestParam(defaultValue = "ASC", value = "direction") String direction
	// fim parametros
	) {
		Page<Cliente> list = service.findPage(page, linesPerPage, orderBy, direction);
		Page<ClienteDTO> listDTO = list.map(obj -> new ClienteDTO(obj));
		return ResponseEntity.ok().body(listDTO);
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> insert(@Valid @RequestBody ClienteNewDTO objDTO) throws EmailException {
		String newPass = objDTO.getSenha();
		Cliente obj = service.fromDTO(objDTO);
		obj.setStatus(false);
		obj = service.insert(obj,newPass);
		obj.setFirstAcess(false);

		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}
 
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> update(@Valid @RequestBody ClienteDTO objDTO, @PathVariable Integer id) {
		Cliente obj = service.fromDTO(objDTO);
		obj.setId(id);
		obj = service.update(obj);
		return ResponseEntity.noContent().build();
	}
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable Integer id) {
		service.delete(id);
		return ResponseEntity.noContent().build();
	}
	
	
	@RequestMapping(value="/picture",method = RequestMethod.POST)
	public ResponseEntity<Void> uploadProfilePicture(@RequestParam(name="file") MultipartFile file){
		URI uri =  service.uploadProfilePicture(file);
		return ResponseEntity.created(uri).build();
	}
	
}





















