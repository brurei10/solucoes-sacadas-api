package br.com.neja.resources;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.validation.Valid;

import org.apache.commons.mail.EmailException;
import org.apache.tomcat.util.bcel.Const;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.neja.domain.Cliente;
import br.com.neja.domain.EnderecoEntrega;
import br.com.neja.domain.ItemPedido;
import br.com.neja.domain.Pedido;
import br.com.neja.repositories.ClienteRepository;
import br.com.neja.repositories.EnderecoEntregaRepository;
import br.com.neja.resources.util.Constants;
import br.com.neja.services.ClienteService;
import br.com.neja.services.PedidoService;
import br.com.neja.services.mail.AbstractEmailService;

@RestController
@RequestMapping(value = "/pedidos")
public class PedidoResource {

	int valid = 0;

	@Autowired
	private PedidoService service;

	@Autowired
	private ClienteRepository repo;

	@Autowired
	private EnderecoEntregaRepository repoendo;
	
	@Autowired
	private ClienteService serviceCliente;

	@Autowired
	private AbstractEmailService emailService;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Pedido> find(@PathVariable Integer id) {
		Pedido obj = service.find(id);
		Integer idCliente = service.findByIdPedido(obj.getId());
		if (idCliente == null) {

		} else {
			Cliente clilocal =  serviceCliente.findById(idCliente);
			if(clilocal==null) {
				
			}else {
				EnderecoEntrega end = repoendo.findByIdCliente(clilocal.getId());
				obj.setEnderecoDeEntrega(end);
				
				obj.setCliente(clilocal);
			}
			
		}

		return ResponseEntity.ok().body(obj);

	}
	
	@RequestMapping(value = "/items/{id}", method = RequestMethod.GET)
	public ResponseEntity<Set<ItemPedido>> findItens(@PathVariable String id) {
		Pedido obj = service.find(Integer.parseInt(id));
		Integer idCliente = service.findByIdPedido(obj.getId());
		if (idCliente == null) {

		} else {
			Cliente clilocal =  serviceCliente.findById(idCliente);
			if(clilocal==null) {
				
			}else {
				EnderecoEntrega end = repoendo.findByIdCliente(clilocal.getId());
				obj.setEnderecoDeEntrega(end);
				
				obj.setCliente(clilocal);
			}
			
		}

		return ResponseEntity.ok().body(obj.getItens());

	}


	@RequestMapping(value = "/funcionario/{email}", method = RequestMethod.GET)
	public ResponseEntity<List<Pedido>> findbyFuncionarioOrcamento(@PathVariable String email) {
		Cliente cli = serviceCliente.findByEmailResource(email);

		List<Pedido> obj = service.findByFuncionarioOrcamento(cli.getId(), Constants.ORCAMENTO);

		for (int i = 0; i < obj.size(); i++) {
			Integer idCliente = service.findByIdPedido(obj.get(i).getId());
			if (idCliente == null) {

			} else {
				Cliente clilocal =  serviceCliente.findById(idCliente);
				if(clilocal==null) {
					
				}else {
					EnderecoEntrega end = repoendo.findByIdCliente(clilocal.getId());
					obj.get(i).setEnderecoDeEntrega(end);
					
					obj.get(i).setCliente(clilocal);
				}
				
			}
		}

		return ResponseEntity.ok().body(obj);

	}

	@RequestMapping(value = "/orcamento/{id}", method = RequestMethod.GET)
	public ResponseEntity<Void> deleteOrcamento(@PathVariable String id) {
		service.delete(Integer.parseInt(id));
		return ResponseEntity.noContent().build();
	}

	@RequestMapping(value = "/pedido/{id}", method = RequestMethod.GET)
	public ResponseEntity<Void> deletePedido(@PathVariable String id) {
		service.delete(Integer.parseInt(id));
		return ResponseEntity.noContent().build();
	}

	@RequestMapping(value = "/funcionario/pedido/{email}", method = RequestMethod.GET)
	public ResponseEntity<List<Pedido>> findbyFuncionarioPedido(@PathVariable String email) {
		Cliente cli = serviceCliente.findByEmailResource(email);

		List<Pedido> obj = service.findByFuncionarioPedido(cli.getId(), Constants.PEDIDO);
		return ResponseEntity.ok().body(obj);

	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> insert(@Valid @RequestBody Pedido obj) throws EmailException {
		// Optional<Cliente> objCliente = repo.findById(obj.getCliente().getId());
		Cliente objCliente = obj.getCliente();
		Cliente objFuncionario = serviceCliente.findByEmailResource(obj.getEmailFuncionario());
		obj.setStatus(Constants.ORCAMENTO);
		obj.setIdFuncionario(objFuncionario.getId());
		obj.setCliente(objCliente);
		obj = service.insert(obj);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId()).toUri();

		return ResponseEntity.created(uri).build();

	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Page<Pedido>> findPage(
			// parametros
			@RequestParam(defaultValue = "0", value = "page") Integer page,
			@RequestParam(defaultValue = "24", value = "linesPerPage") Integer linesPerPage,
			@RequestParam(defaultValue = "instante", value = "orderBy") String orderBy,
			@RequestParam(defaultValue = "DESC", value = "direction") String direction
	// fim parametros
	) {
		Page<Pedido> list = service.findPage(page, linesPerPage, orderBy, direction);

		return ResponseEntity.ok().body(list);
	}

	@RequestMapping(value = "/sendpedido/{id}", method = RequestMethod.GET)
	public ResponseEntity<Pedido> updateToPedido(@PathVariable String id) {

		Pedido obj = service.find(Integer.parseInt(id));
		obj.setStatus(Constants.PEDIDO);
		obj = service.update(obj);
		return ResponseEntity.ok().body(obj);
	}

}
