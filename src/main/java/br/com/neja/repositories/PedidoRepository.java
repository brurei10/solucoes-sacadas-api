package br.com.neja.repositories;

 

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.neja.domain.Cliente;
import br.com.neja.domain.Pedido;
import br.com.neja.resources.util.Constants;

@Repository
public interface PedidoRepository extends JpaRepository<Pedido, Integer> , CrudRepository<Pedido, Integer>{

	
	@Transactional(readOnly= true)
	Page<Pedido> findByCliente(Cliente obj,Pageable pageRequest);
	
	@Query(value = "SELECT * FROM Pedido p WHERE p.id_funcionario_cliente = ?1 and status = ?2" ,nativeQuery = true)
	List<Pedido> findByFuncionarioOrcamento(Integer idFuncionario,String orcamento);
	
	@Query(value = "SELECT * FROM Pedido p WHERE p.id_funcionario_cliente = ?1 and status = ?2",nativeQuery = true)
	List<Pedido> findByFuncionarioPedido(Integer idFuncionario, String pedido);
	
	@Query(value = "SELECT p.id_cliente FROM Pedido p WHERE p.id_pedido = ?1 ",nativeQuery = true)
	Integer findByIdPedido(Integer id);
	 
	@Query(value = "UPDATE Pedido set id_cliente =?1 where id_pedido = ?2",
	            nativeQuery = true)
	void updatePedidoCliente(Integer id, Integer pedidoId);
	

	
}
