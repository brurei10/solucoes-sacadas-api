package br.com.neja.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.neja.domain.Cliente;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Integer>, CrudRepository<Cliente, Integer> {

	
	@Transactional(readOnly=true)
	Cliente findByEmail(String email);
	
	@Query(value = "SELECT * FROM CLIENTE u WHERE u.cpf_ou_cnpj = ?1", nativeQuery = true)
	Cliente findByCpf(String cpf);
	
}
