package br.com.neja.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.neja.domain.EnderecoEntrega;

@Repository
public interface EnderecoEntregaRepository extends JpaRepository<EnderecoEntrega, Integer>, CrudRepository<EnderecoEntrega, Integer> {

	@Query(value = "SELECT * FROM ENDERECO_ENTREGA u WHERE u.id_cliente = ?1", nativeQuery = true)
	EnderecoEntrega findByIdCliente(Integer id);
}
