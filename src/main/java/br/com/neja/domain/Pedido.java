package br.com.neja.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class Pedido implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_PEDIDO")
	private Integer id;

	@JsonFormat(pattern = "dd/MM/yyyy HH:mm")
	private LocalDateTime instante;

	@OneToOne(cascade = CascadeType.ALL, mappedBy = "pedido")
	private Pagamento pagamento;

	@ManyToOne(cascade = {CascadeType.ALL})
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "ID_ENDERECO_ENTREGA")
	private EnderecoEntrega enderecoDeEntrega; 

	@ManyToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "ID_CLIENTE")
	 @OnDelete(action = OnDeleteAction.CASCADE)
	private Cliente cliente;

	@Column(name = "ID_FUNCIONARIO_CLIENTE")
	private Integer idFuncionario;
	
	@Column(name = "EMAIL_FUNCIONARIO_CLIENTE")
	private String emailFuncionario;

	@OneToMany(mappedBy = "id.pedido")
	private Set<ItemPedido> itens = new HashSet<>();

	@Column(name = "STATUS")
	private String status;
	
	@Column(name = "TOTAL")
	private Long total;

	@Column(name = "KIT_SOLUCAO", nullable = true)
	private String kitSolucao;
	@Column(name = "CUSTO_VIDRO", nullable = true)
	private String custoVidro;
	@Column(name = "LARGURA", nullable = true)
	private String larguraTotal;
	@Column(name = "ALTURA", nullable = true)
	private String alturaTotal;
	@Column(name = "VALOR_KIT_METRO", nullable = true)
	private String valorKitMetro;
	@Column(name = "VALOR_VIDRO", nullable = true)
	private String valorVidro;
	@Column(name = "META_MARGEM", nullable = true)
	private String metaMargem;
	@Column(name = "VALOR_FIM", nullable = true)
	private String valorFim;
	@Column(name = "COD_PRODUTO", nullable = true)
	private String codProduto;

	
	
	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public Pedido() {

	}

	public Pedido(Integer id, LocalDateTime instante, Pagamento pagamento, Cliente cliente,
			EnderecoEntrega enderecoDeEntrega) {
		super();
		this.id = id;
		this.instante = instante;
		this.pagamento = pagamento;
		this.cliente = cliente;
		this.enderecoDeEntrega = enderecoDeEntrega;
	}

	public Pedido(Integer id, LocalDateTime instante, Cliente cliente, EnderecoEntrega enderecoDeEntrega) {
		super();
		this.id = id;
		this.instante = instante;
		this.cliente = cliente;
		this.enderecoDeEntrega = enderecoDeEntrega;
	}

	public BigDecimal getValorTotal() {
		BigDecimal soma = BigDecimal.ZERO;

		for (ItemPedido ped : itens) {

			soma = soma.add(ped.getSubTotal());
		}

		return soma;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public LocalDateTime getInstante() {
		return instante;
	}

	public void setInstante(LocalDateTime instante) {
		this.instante = instante;
	}

	public Pagamento getPagamento() {
		return pagamento;
	}

	public void setPagamento(Pagamento pagamento) {
		this.pagamento = pagamento;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public EnderecoEntrega getEnderecoDeEntrega() {
		return enderecoDeEntrega;
	}

	public void setEnderecoDeEntrega(EnderecoEntrega enderecoDeEntrega) {
		this.enderecoDeEntrega = enderecoDeEntrega;
	}

	public Set<ItemPedido> getItens() {
		return itens;
	}

	public void setItens(Set<ItemPedido> itens) {
		this.itens = itens;
	}

	@Override
	public String toString() {
		NumberFormat nf = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
		
		StringBuilder builder = new StringBuilder();
		builder.append("Pedido número: ");
		builder.append(getId());
		builder.append(", Instante: ");
		builder.append(getInstante());
		builder.append(", Cliente: ");
		builder.append(getCliente().getNome());
		builder.append(", Situação do pagamento: ");
		builder.append(getPagamento().getEstado().getDescricao());
		builder.append("\nDetalhes:\n");
		for (ItemPedido ip : getItens()) {
			builder.append(ip.toString());
		}
		builder.append("Valor total: ");
		builder.append(nf.format(getValorTotal()));
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pedido other = (Pedido) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Integer getIdFuncionario() {
		return idFuncionario;
	}

	public void setIdFuncionario(Integer idFuncionario) {
		this.idFuncionario = idFuncionario;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getEmailFuncionario() {
		return emailFuncionario;
	}

	public void setEmailFuncionario(String emailFuncionario) {
		this.emailFuncionario = emailFuncionario;
	}

	public String getKitSolucao() {
		return kitSolucao;
	}

	public void setKitSolucao(String kitSolucao) {
		this.kitSolucao = kitSolucao;
	}

	public String getCustoVidro() {
		return custoVidro;
	}

	public void setCustoVidro(String custoVidro) {
		this.custoVidro = custoVidro;
	}

	public String getLarguraTotal() {
		return larguraTotal;
	}

	public void setLarguraTotal(String larguraTotal) {
		this.larguraTotal = larguraTotal;
	}

	public String getAlturaTotal() {
		return alturaTotal;
	}

	public void setAlturaTotal(String alturaTotal) {
		this.alturaTotal = alturaTotal;
	}

	public String getValorKitMetro() {
		return valorKitMetro;
	}

	public void setValorKitMetro(String valorKitMetro) {
		this.valorKitMetro = valorKitMetro;
	}

	public String getValorVidro() {
		return valorVidro;
	}

	public void setValorVidro(String valorVidro) {
		this.valorVidro = valorVidro;
	}

	public String getMetaMargem() {
		return metaMargem;
	}

	public void setMetaMargem(String metaMargem) {
		this.metaMargem = metaMargem;
	}

	public String getValorFim() {
		return valorFim;
	}

	public void setValorFim(String valorFim) {
		this.valorFim = valorFim;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getCodProduto() {
		return codProduto;
	}

	public void setCodProduto(String codProduto) {
		this.codProduto = codProduto;
	}
	
	

}
