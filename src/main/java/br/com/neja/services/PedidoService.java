package br.com.neja.services;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.apache.commons.mail.EmailException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.neja.domain.Cliente;
import br.com.neja.domain.Endereco;
import br.com.neja.domain.EnderecoEntrega;
import br.com.neja.domain.ItemPedido;
import br.com.neja.domain.PagamentoComBoleto;
import br.com.neja.domain.Pedido;
import br.com.neja.domain.enums.EstadoPagamento;
import br.com.neja.repositories.ClienteRepository;
import br.com.neja.repositories.EnderecoEntregaRepository;
import br.com.neja.repositories.EnderecoRepository;
import br.com.neja.repositories.ItemPedidoRepository;
import br.com.neja.repositories.PagamentoRepository;
import br.com.neja.repositories.PedidoRepository;
import br.com.neja.security.UserSS;
import br.com.neja.services.exception.AuthorizationException;
import br.com.neja.services.exception.DataIntegrityException;
import br.com.neja.services.exception.ObjectNotFoundException;
import br.com.neja.services.mail.AbstractEmailService;

@Service
public class PedidoService {
	@Autowired
	private PedidoRepository repo;
	
	@Autowired
	private EnderecoEntregaRepository repoend;

	@Autowired
	private ItemPedidoRepository itemRepo;
	
	@Autowired
	private ClienteRepository clirepo;

	@Autowired
	private BoletoService boletoService;

	@Autowired
	private PagamentoRepository pgRepo;

	@Autowired
	private ProdutoService produtoService;

	
	@Autowired
	private ClienteService clienteService;
	
	@Autowired
	private AbstractEmailService emailService;

	public Pedido find(Integer id) {
		Optional<Pedido> obj = repo.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id:" + id + " Tipo: " + Pedido.class.getSimpleName()));
	}
	
	public List<Pedido> findByFuncionarioOrcamento(Integer id, String orcamento) {
		List<Pedido> obj = repo.findByFuncionarioOrcamento(id, orcamento);
		return obj;
				}
	
	public Integer findByIdPedido(Integer id) {
		Integer obj = repo.findByIdPedido(id);
		return obj;
				}

	public List<Pedido> findByFuncionarioPedido(Integer id, String pedido) {
		List<Pedido> obj = repo.findByFuncionarioPedido(id, pedido);
		return obj;
				}

	
	
	public Pedido insert(Pedido obj) {
		Cliente objCli = clirepo.findByEmail(obj.getCliente().getEmail());
		EnderecoEntrega end = null;

		if(objCli!=null) {
			obj.setCliente(objCli);
			end = repoend.findByIdCliente(objCli.getId());
			
			if(end==null) {
				end =  new EnderecoEntrega();
				end.setCliente(obj.getCliente());
				end.setEstado(obj.getEnderecoDeEntrega().getEstado());
				end.setId(null);
				end.setBairro(obj.getEnderecoDeEntrega().getBairro());
				end.setCep(obj.getEnderecoDeEntrega().getCep());
				end.setCidade(obj.getEnderecoDeEntrega().getCidade());
				end.setComplemento(obj.getEnderecoDeEntrega().getComplemento());
				end.setLogradouro(obj.getEnderecoDeEntrega().getLogradouro());
				end.setNumero(obj.getEnderecoDeEntrega().getNumero());
				
				end = repoend.save(end);
					
			}
			
			
		}else {
			end =  new EnderecoEntrega();
			end.setCliente(obj.getCliente());
			end.setEstado(obj.getEnderecoDeEntrega().getEstado());
			end.setId(null);
			end.setBairro(obj.getEnderecoDeEntrega().getBairro());
			end.setCep(obj.getEnderecoDeEntrega().getCep());
			end.setCidade(obj.getEnderecoDeEntrega().getCidade());
			end.setComplemento(obj.getEnderecoDeEntrega().getComplemento());
			end.setLogradouro(obj.getEnderecoDeEntrega().getLogradouro());
			end.setNumero(obj.getEnderecoDeEntrega().getNumero());
			
			end = repoend.save(end);
			
		}
		//if(objCli.getEnderecos().get(0)==null) {
		
		
		obj.setId(null);
		obj.setEnderecoDeEntrega(end);
		obj.setInstante(LocalDateTime.now());
		obj.getPagamento().setEstado(EstadoPagamento.PENDENTE);
		obj.getPagamento().setPedido(obj);

		if (obj.getPagamento() instanceof PagamentoComBoleto) {
			PagamentoComBoleto pgto = (PagamentoComBoleto) obj.getPagamento();
			boletoService.preencherPagamentoBoleto(pgto, obj.getInstante());
		}
		
		
		Cliente possave = obj.getCliente();
		EnderecoEntrega pos = obj.getEnderecoDeEntrega();
		obj.setCliente(null);
		obj.setEnderecoDeEntrega(null);
		obj = repo.saveAndFlush(obj);
		pgRepo.saveAndFlush(obj.getPagamento());
		for (ItemPedido item : obj.getItens()) {
			item.setPedido(obj);
			item.setProduto(produtoService.find(item.getProduto().getId()));
			item.setDesconto(BigDecimal.ZERO);
			item.setPreco(produtoService.find(item.getProduto().getId()).getPreco());

		}

		itemRepo.saveAll(obj.getItens());
		try {
			obj.setCliente(possave);
			obj.setEnderecoDeEntrega(pos);
			emailService.sendOrderConfirmationEmail(obj);
		    //obj.setEnderecoDeEntrega(null);
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // ta dando erro
		
		update(possave, obj,pos);
		
		return obj;
	}

	
	public void update(Cliente possave,Pedido obj, EnderecoEntrega pos) {
		
		try {
			repo.updatePedidoCliente(possave.getId(), obj.getId());
				
		} catch (Exception e) {
			
		}
			
				
	
		
	}

	
	public Page<Pedido> findPage(
//			 parametros
			Integer page,
			Integer linesPerPage,
			String orderBy,
			String direction
//			fim parametros
			){
		
		UserSS user = UserService.authenticated();
		if (user == null) 
		throw  new AuthorizationException("Acesso Negado");
		
		PageRequest pageRequest =  PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		
		Cliente cliTemp = clienteService.find(user.getId());
		
		return repo.findByCliente(cliTemp, pageRequest);
	}
	
	
	public void delete(Integer id) {
		Pedido c = find(id);
		try {
			repo.deleteById(c.getId());
		} catch (DataIntegrityViolationException ex) {
			ex.printStackTrace();
			throw new DataIntegrityException(
					"Não é possível fazer a operação DELETE PEDIDO :" + c.getCliente().getNome() + ", existem pedidos relacionados.");
		}

	}
	
	public Pedido update(Pedido obj) {
		Pedido newObj = find(obj.getId());
		updateData(newObj, obj);
		return repo.save(newObj);
	}
	
	private void updateData(Pedido newObj, Pedido obj) {
		newObj.setStatus(obj.getStatus());
	}
	
}
