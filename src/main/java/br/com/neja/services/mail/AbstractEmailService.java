package br.com.neja.services.mail;

import java.util.Date;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import br.com.neja.domain.Cliente;
import br.com.neja.domain.Pedido;

@Service
public class AbstractEmailService {

	//@Value("${default.sender}")
	private String sender = "brurei10@gmail.com";

	// @Autowired
	// private TemplateEngine engine;

	// @Autowired
	// private JavaMailSender javaMailSender;
	//


	public void sendOrderConfirmationEmail(Pedido obj) throws EmailException {
		 prepareSimpleMailMessageFromPedido(obj);
		
	}

	protected void prepareSimpleMailMessageFromPedido(Pedido obj) throws EmailException {
		// Cria o e-mail
		String tipoProduto = "";
		if(obj.getCodProduto().equalsIgnoreCase("1")){
			tipoProduto = "SACADAS";
		}else {
			tipoProduto = "PERSIANAS";
		}
		HtmlEmail email = new HtmlEmail();
		email.setDebug(true);
		email.setHostName("smtp.mailgun.org");
		email.setAuthentication("postmaster@mg.evollo.com.br", "84c7db18f3b66aa0bce6146a044e06a8-ba042922-844c01dd");
		email.setSmtpPort(587);
		email.setSSL(true);
		email.setTLS(true);
		email.addTo(obj.getCliente().getEmail(), obj.getCliente().getNome());
		email.setFrom("nao_responda@solucoes.com.br", "SOLUÇÕES SACADAS - ORÇAMENTO CONFIRMADO");
		email.setSubject("SOLUÇÕES SACADAS - ORÇAMENTO CONFIRMADO");

		
		// configura a mensagem para o formato HTML
		email.setHtmlMsg("<html>\r\n"
				+ "<head></head>\r\n"
				+ "<body><p style=\"text-align: justify;\">"
				+ "</p><div class=\"separator\" style=\"clear: both; text-align: center;\"><br />"
				+ "</div><div class=\"separator\" style=\"clear: both; text-align: center;\">"
				+ "<a href=\"https://claudiaklein.com.br/wp-content/uploads/2018/08/1-A-import%C3%A2ncia-da-Pol%C3%ADtica-de-boas-vindas.png\" imageanchor=\"1\" style=\"margin-left: 1em; margin-right: 1em;\">"
				+ "<img border=\"0\" data-original-height=\"413\" data-original-width=\"800\" height=\"324\" "
				+ "src=\"https://claudiaklein.com.br/wp-content/uploads/2018/08/1-A-import%C3%A2ncia-da-Pol%C3%ADtica-de-boas-vindas.png\" "
				+ "width=\"627\" /></a></div><br /><div class=\"separator\" style=\"clear: both; text-align: center;\">"
				+ "<a href=\"http://solucaosacadas.com.br/wp-content/uploads/2017/05/solucao-sacadas-logo-site.png\" "
				+ "imageanchor=\"1\" style=\"margin-left: 1em; margin-right: 1em;\"><img border=\"0\" data-original-height=\"71\" "
				+ "data-original-width=\"200\" height=\"116\" "
				+ "src=\"http://solucaosacadas.com.br/wp-content/uploads/2017/05/solucao-sacadas-logo-site.png\" width=\"328\" /></a>"
				+ "</div><div class=\"separator\" style=\"clear: both; text-align: center;\"><br /></div><p></p>"
				+ "<p style=\"text-align: justify;\"><span style=\"font-family: 'Exo 2';\">Olá "+obj.getCliente().getNome()+"!"
						+ " Seu orçamento foi confirmado.&nbsp;</span></p><p style=\"text-align: justify;\"><span style=\"font-family: 'Exo 2';\">"
						+ "Segue abaixo o detalhamento:</span></p><p style=\"text-align: justify;\"><span style=\"font-family: 'Exo 2';\">"
						+ "DADOS DO ORÇAMENTO: "+tipoProduto+"</span></p><p style=\"text-align: justify;\"><span style=\"font-family: 'Exo 2';\"></span>"
						+ "</p><p style=\"text-align: justify;\"><span style=\"font-family: 'Exo 2';\"></span>"
						+ "</p><p style=\"text-align: justify;\"><span style=\"font-family: 'Exo 2'; font-size: medium;\">"
								+ "<span style=\"font-family: 'Exo 2'; font-size: medium;\">"
								+ "<b>Largura Total:"+obj.getLarguraTotal()+"</p>"
										+ "<b>Altura Total:"+obj.getAlturaTotal()+"</p><br>"
												+ "<b>Valor Kit Metro Linear (R$):"+obj.getValorKitMetro()+"</p><br>"
														+ "<b>Valor do Vidro por m² (R$):"+obj.getValorVidro()+"</p><br>"
																		+ "<center><b>Resultados</center></p><br>"
																		+ "<b>Custo Total:"+obj.getValorTotal()+"</p><br>"
																				+ "<b>Kit Solução R$:"+obj.getKitSolucao()+"</p><br>"
																						+ "<b>Custo Vidros R$:"+obj.getCustoVidro()+"</p><br>"
																								+ "<b>Custo Acessórios R$:"+obj.getTotal()+"</p><br>"




												
														+ "<b>ACESSÓRIOS :"+obj.getItens()+"</b></span></p><p style=\"text-align: justify;\">"
																+ "<b>ALTURA TOTAL:"+obj.getAlturaTotal()+"</p>"



								+ "<b>ENDEREÇO DE ENTREGA:"+obj.getEnderecoDeEntrega().getCep()+"</p>"
										+ ""+obj.getEnderecoDeEntrega().getLogradouro()+"</p>"+obj.getEnderecoDeEntrega().getNumero()+"&nbsp;</p>"
												+ "</b></span></p></body>");

		// configure uma mensagem alternativa caso o servidor não suporte HTML
		email.setTextMsg("Seu servidor de e-mail não suporta mensagem HTML");

		// envia o e-mail
		email.send();

	     

		
		}
	
	
	public void sendNewUserEmail(Cliente cliente) throws EmailException {
		 prepareNewUserEmail(cliente);
		
	}

	protected void prepareNewUserEmail(Cliente obj) throws EmailException {
		// Cria o e-mail
		HtmlEmail email = new HtmlEmail();
		email.setDebug(true);
		email.setHostName("smtp.mailgun.org");
		email.setAuthentication("postmaster@mg.evollo.com.br", "84c7db18f3b66aa0bce6146a044e06a8-ba042922-844c01dd");
		email.setSmtpPort(587);
		email.setSSL(true);
		email.setTLS(true);
		email.addTo(obj.getEmail(), obj.getNome());
					email.setFrom("nao_responda@solucoes.com.br", "SOLUÇÕES SACADAS - SOLICITAÇÃO CADASTRO");
					email.setSubject("SOLUÇÕES SACADAS - SOLICITAÇÃO CADASTRO");

					// configura a mensagem para o formato HTML
					email.setHtmlMsg("<html>\r\n"
							+ "<head></head>\r\n"
							+ "<body><p style=\"text-align: justify;\"></p><div class=\"separator\" style=\"clear: both; text-align: center;\"><br /></div><div class=\"separator\" style=\"clear: both; text-align: center;\"><a href=\"https://claudiaklein.com.br/wp-content/uploads/2018/08/1-A-import%C3%A2ncia-da-Pol%C3%ADtica-de-boas-vindas.png\" imageanchor=\"1\" style=\"margin-left: 1em; margin-right: 1em;\"><img border=\"0\" data-original-height=\"413\" data-original-width=\"800\" height=\"324\" src=\"https://claudiaklein.com.br/wp-content/uploads/2018/08/1-A-import%C3%A2ncia-da-Pol%C3%ADtica-de-boas-vindas.png\" width=\"627\" /></a></div><br /><div class=\"separator\" style=\"clear: both; text-align: center;\"><a href=\"http://solucaosacadas.com.br/wp-content/uploads/2017/05/solucao-sacadas-logo-site.png\" imageanchor=\"1\" style=\"margin-left: 1em; margin-right: 1em;\"><img border=\"0\" data-original-height=\"71\" data-original-width=\"200\" height=\"116\" src=\"http://solucaosacadas.com.br/wp-content/uploads/2017/05/solucao-sacadas-logo-site.png\" width=\"328\" /></a></div><div class=\"separator\" style=\"clear: both; text-align: center;\"><br /></div><p></p><p style=\"text-align: justify;\"><span style=\"font-family: 'Exo 2';\">Bem vindo "+obj.getNome()+"! Estamos muito felizes por você felizes por você possuir interesse em fazer parte da família Soluções.&nbsp;</span></p><p style=\"text-align: justify;\"><span style=\"font-family: 'Exo 2';\">Nossa equipe está validando suas informações, e caso estejam todas aderentes aos nossos quesitos, estaremos encaminhando um novo email de confirmação e, com este, você poderá iniciar o acesso ao nosso APP.</span></p><p style=\"text-align: justify;\"><span style=\"font-family: 'Exo 2';\">Fique calmo, este processo demora no máximo 24 horas.</span></p><p style=\"text-align: justify;\"><span style=\"font-family: 'Exo 2';\">Abaixo seguem as informações principais para que você possa efetuar seu login no aplicativo após nossa liberação:</span></p><p style=\"text-align: justify;\"><span style=\"font-family: 'Exo 2';\">Lembre que esses dados são privados e não devem ser compartilhados:</span></p><p style=\"text-align: justify;\"><span style=\"font-family: 'Exo 2'; font-size: medium;\"><b>Email :"+obj.getEmail()+"</b></span></p><p style=\"text-align: justify;\"><span style=\"font-family: 'Exo 2'; font-size: medium;\"><b>Senha:"+obj.getSenha()+"&nbsp;</b></span></p></body>");

					// configure uma mensagem alternativa caso o servidor não suporte HTML
					email.setTextMsg("Seu servidor de e-mail não suporta mensagem HTML");

					// envia o e-mail
					email.send();
	     

		
		}


	// @Override
	// public void sendOrderConfirmationHtmlEmail(Pedido obj) {
	// try {
	// MimeMessage mm = prepareMimeMessageFromPedido(obj);
	// sendHtmlEmail(mm);
	// } catch (Exception e) {
	// sendOrderConfirmationEmail(obj);
	// }
	//
	// }

	// protected MimeMessage prepareMimeMessageFromPedido(Pedido obj) throws
	// MessagingException {
	// MimeMessage mimeMessage = javaMailSender.createMimeMessage();
	// MimeMessageHelper mmh = new MimeMessageHelper(mimeMessage, true);
	// mmh.setTo(obj.getCliente().getEmail());
	// mmh.setFrom(sender);
	// mmh.setSentDate(new Date(System.currentTimeMillis()));
	// mmh.setSubject("Pedido cadaqstrado com sucesso "+obj.getId());
	// mmh.setText(htmlFromTemplatePedido(obj),true);
	// return mimeMessage;
	// }

	// protected String htmlFromTemplatePedido(Pedido obj) {
	// Context context = new Context();
	// context.setVariable("pedido", obj);
	// return engine.process("email/confirmacao-pedido", context);
	//
	// }
	
	public void sendNewPasswordEmail(Cliente cliente, String newPass) {
		 try {
			prepareNewPasswordEmail(cliente,newPass);
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	protected void prepareNewPasswordEmail(Cliente obj, String newPass) throws EmailException {
		HtmlEmail email = new HtmlEmail();
		email.setDebug(true);
		email.setHostName("smtp.mailgun.org");
		email.setAuthentication("postmaster@mg.evollo.com.br", "84c7db18f3b66aa0bce6146a044e06a8-ba042922-844c01dd");
		email.setSmtpPort(587);
		email.setSSL(true);
		email.setTLS(true);
		email.addTo(obj.getEmail(), obj.getNome());
		email.setFrom("nao_responda@solucoes.com.br", "SOLUÇÕES SACADAS - RECUPERAÇÃO DE SENHA");
		email.setSubject("SOLUÇÕES SACADAS - SOLICITAÇÃO NOVA SENHA");

		// configura a mensagem para o formato HTML
		email.setHtmlMsg("<html>\r\n"
				+ "<head></head>\r\n"
				+ "<body><p style=\"text-align: justify;\"></p><div class=\"separator\" style=\"clear: both; text-align: center;\"><br /></div><div class=\"separator\" style=\"clear: both; text-align: center;\"><a href=\"https://claudiaklein.com.br/wp-content/uploads/2018/08/1-A-import%C3%A2ncia-da-Pol%C3%ADtica-de-boas-vindas.png\" imageanchor=\"1\" style=\"margin-left: 1em; margin-right: 1em;\"><img border=\"0\" data-original-height=\"413\" data-original-width=\"800\" height=\"324\" src=\"https://claudiaklein.com.br/wp-content/uploads/2018/08/1-A-import%C3%A2ncia-da-Pol%C3%ADtica-de-boas-vindas.png\" width=\"627\" /></a></div><br /><div class=\"separator\" style=\"clear: both; text-align: center;\"><a href=\"http://solucaosacadas.com.br/wp-content/uploads/2017/05/solucao-sacadas-logo-site.png\" imageanchor=\"1\" style=\"margin-left: 1em; margin-right: 1em;\"><img border=\"0\" data-original-height=\"71\" data-original-width=\"200\" height=\"116\" src=\"http://solucaosacadas.com.br/wp-content/uploads/2017/05/solucao-sacadas-logo-site.png\" width=\"328\" /></a></div><div class=\"separator\" style=\"clear: both; text-align: center;\"><br /></div><p></p><p style=\"text-align: justify;\"><span style=\"font-family: 'Exo 2';\">Bem vindo "+obj.getNome()+"! Recebemos uma solicitação de nova senha.&nbsp;</span></p><p style=\"text-align: justify;\"><span style=\"font-family: 'Exo 2';\">.</span></p><p style=\"text-align: justify;\"><span style=\"font-family: 'Exo 2';\"></span></p><p style=\"text-align: justify;\"><span style=\"font-family: 'Exo 2';\">Abaixo seguem as informações principais para que você possa efetuar seu login no aplicativo após sua solicitação. Vale lembrar que será solicitada a atualização no primeiro acesso:</span></p><p style=\"text-align: justify;\"><span style=\"font-family: 'Exo 2';\">Lembre que esses dados são privados e não devem ser compartilhados:</span></p><p style=\"text-align: justify;\"><span style=\"font-family: 'Exo 2'; font-size: medium;\"><b>Email :"+obj.getEmail()+"</b></span></p><p style=\"text-align: justify;\"><span style=\"font-family: 'Exo 2'; font-size: medium;\"><b>Senha:"+newPass+"&nbsp;</b></span></p></body>");

		// configure uma mensagem alternativa caso o servidor não suporte HTML
		email.setTextMsg("Seu servidor de e-mail não suporta mensagem HTML");

		// envia o e-mail
		email.send();

	}
	
	}
	
/*public static void main(String args[]) throws EmailException {
	AbstractEmailService ab = new AbstractEmailService();
	Cliente obj =  new Cliente();
	obj.setNome("Teste");
	obj.setEmail("brurei10@gmail.com");
	obj.setSenha("123456");
	ab.prepareNewUserEmail(obj);
	
}*/
