package br.com.neja.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.neja.domain.Cidade;
import br.com.neja.domain.Estado;
import br.com.neja.repositories.CidadeRepository;

@Service
public class CidadeService {

	@Autowired
	private CidadeRepository repo;
	
	public List<Cidade> findByEstado(Integer id){
		return repo.findCidades(id);
	}
	
public Optional<Cidade> findById(Integer id) {
		
		return repo.findById(id);
	}

public Cidade findByName(String name) {
	
	return repo.findCidadesName(name);
}
}
