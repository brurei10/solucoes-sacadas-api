package br.com.neja.services;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.neja.domain.Categoria;
import br.com.neja.domain.Cidade;
import br.com.neja.domain.Cliente;
import br.com.neja.domain.Endereco;
import br.com.neja.domain.EnderecoEntrega;
import br.com.neja.domain.Estado;
import br.com.neja.domain.ItemPedido;
import br.com.neja.domain.Pagamento;
import br.com.neja.domain.PagamentoComBoleto;
import br.com.neja.domain.PagamentoComCartao;
import br.com.neja.domain.Pedido;
import br.com.neja.domain.Produto;
import br.com.neja.domain.enums.EstadoPagamento;
import br.com.neja.domain.enums.Perfil;
import br.com.neja.domain.enums.TipoCliente;
import br.com.neja.repositories.CategoriaRepository;
import br.com.neja.repositories.CidadeRepository;
import br.com.neja.repositories.ClienteRepository;
import br.com.neja.repositories.EnderecoRepository;
import br.com.neja.repositories.EstadoRepository;
import br.com.neja.repositories.ItemPedidoRepository;
import br.com.neja.repositories.PagamentoRepository;
import br.com.neja.repositories.PedidoRepository;
import br.com.neja.repositories.ProdutoRepository;


@Service
public class DBService {
	@Autowired
	private CategoriaRepository catRepo;

	@Autowired
	private ProdutoRepository prodRepo;

	@Autowired
	private EstadoRepository estRepo;

	@Autowired
	private EnderecoRepository endRepo;

	@Autowired
	private ClienteRepository cliRepo;

	@Autowired
	private CidadeRepository cidRepo;

	@Autowired
	private PagamentoRepository pagRepo;

	@Autowired
	private PedidoRepository pedRepo;
	
	@Autowired
	private ItemPedidoRepository itemRepo;

	@Autowired
	private BCryptPasswordEncoder crypt;
	
	public void initTestDatabase() {
		// CATEGORIA E PRODUTO

		Categoria c1 = new Categoria("Sacadas","assets/imgs/logo-sacada.png");
		Categoria cat1 = new Categoria("Persianas","assets/imgs/logo-persiana.png");
		Categoria c2 = new Categoria("Pele de Vidro","assets/imgs/logo-door.jpg");
		Categoria c3 = new Categoria("Truck","assets/imgs/logo-truck.png");
		

		Produto p1 = new Produto(null, "Tampão (Perfil de Leito)", new BigDecimal("1200"));
		Produto p2 = new Produto(null, "Kit sem haste", new BigDecimal("800"));
		Produto p3 = new Produto(null, "Perfil de silicone", new BigDecimal("10"));
		Produto p4 = new Produto(null, "Aparador de inox", new BigDecimal("1200"));
		Produto p5 = new Produto(null, "Aparador de Alumínio", new BigDecimal("800"));
		Produto p6 = new Produto(null, "Fachadura Vidro/Vidro", new BigDecimal("10"));
		Produto p7 = new Produto(null, "Chapa de Correção", new BigDecimal("1200"));
		
		Produto p8 = new Produto(null, "Branco", new BigDecimal("800"));
		Produto p9 = new Produto(null, "Natural Fosco", new BigDecimal("10"));
		Produto p10 = new Produto(null, "Preto", new BigDecimal("1200"));
		Produto p11= new Produto(null, "Bz 1001", new BigDecimal("800"));
		Produto p12= new Produto(null, "Bz 1002", new BigDecimal("800"));
		Produto p13= new Produto(null, "Bz 1003", new BigDecimal("800"));
		Produto p14= new Produto(null, "Outros", new BigDecimal("600"));
		Produto p15= new Produto(null, "Incolor", new BigDecimal("3500"));
		Produto p16= new Produto(null, "Verde", new BigDecimal("800"));
		Produto p17= new Produto(null, "Bronze", new BigDecimal("800"));
		Produto p18= new Produto(null, "Cinza", new BigDecimal("400"));
		Produto p19= new Produto(null, "8", new BigDecimal("200"));
		Produto p20= new Produto(null, "10", new BigDecimal("800"));
		
		
		
		cat1.getProdutos().addAll(Arrays.asList(p8,p9,p10,p11,p12,p13,p14,p15,p16,p17,p18,p19,p20));
		c1.getProdutos().addAll(Arrays.asList(p1, p2, p3, p4, p5, p6, p7));
		c2.getProdutos().addAll(Arrays.asList(p1, p2));
		
		p1.getCategorias().addAll(Arrays.asList( c1));
		p2.getCategorias().addAll(Arrays.asList( c1));
		p3.getCategorias().addAll(Arrays.asList( c1));
		p4.getCategorias().addAll(Arrays.asList( c1));
		p5.getCategorias().addAll(Arrays.asList( c1));
		p6.getCategorias().addAll(Arrays.asList( c1));
		p7.getCategorias().addAll(Arrays.asList( c1));
		
		p8.getCategorias().addAll(Arrays.asList( cat1));
		p9.getCategorias().addAll(Arrays.asList( cat1));
		
		p10.getCategorias().addAll(Arrays.asList( cat1));
		p11.getCategorias().addAll(Arrays.asList( cat1));
		p12.getCategorias().addAll(Arrays.asList( cat1));
		p13.getCategorias().addAll(Arrays.asList( cat1));
		p15.getCategorias().addAll(Arrays.asList( cat1));
		p16.getCategorias().addAll(Arrays.asList( cat1));
		p17.getCategorias().addAll(Arrays.asList( cat1));
		p18.getCategorias().addAll(Arrays.asList( cat1));
		p19.getCategorias().addAll(Arrays.asList( cat1));
		p20.getCategorias().addAll(Arrays.asList( cat1));
		p14.getCategorias().addAll(Arrays.asList( cat1));
		
		catRepo.saveAll(Arrays.asList(c1,cat1,c3, c2));
		
		prodRepo.saveAll(Arrays.asList(p1, p2, p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17,p18,p19,p20));

		// ESTADO E CIDADE

		Estado mt = new Estado(111, "Mato grosso", "MT");
		Estado sp = new Estado(222, "Sao Paulo", "SP");

		Cidade cd1 = new Cidade(null,255, "Cuiaba", mt);
		Cidade cd2 = new Cidade(null,245, "Sorocaba", sp);

		mt.getCidades().addAll(Arrays.asList(cd1));
		sp.getCidades().addAll(Arrays.asList(cd2));

		estRepo.saveAll(Arrays.asList(mt, sp));
		cidRepo.saveAll(Arrays.asList(cd1, cd2));

		// CLIENTE E ENDEREÇO

		Cliente cl1 = new Cliente(null, "admin", "brurei10@gmail.com", "123456", TipoCliente.PESSOA_FISICA,crypt.encode("123456"));
		cl1.getTelefones().addAll(Arrays.asList("9 99896554", "9 65895421"));
		cl1.addPerfil(Perfil.ADMIN);
		EnderecoEntrega e1 = new EnderecoEntrega(null, "bem ali", "123", "A","Pará", "Belém", "centro", "78026888", cl1);
		//cl1.getEnderecos().addAll(Arrays.asList(e1));

		Cliente cl2 = new Cliente(null, "root", "ze@ze.com", "888345678996", TipoCliente.PESSOA_FISICA,crypt.encode("123"));
		cl2.getTelefones().addAll(Arrays.asList("9 45789963", "9 21547898"));
		Endereco e2 = new Endereco(null, "bem ali", "555", "B", cd2, "vila maria", "78076888", cl2);
		cl1.getEnderecos().addAll(Arrays.asList(e2));

		cliRepo.saveAll(Arrays.asList(cl2, cl1));
		//endRepo.saveAll(Arrays.asList(e1, e2));

		// PEDIDO E PAGAMENTO
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
		Pedido pd1 = new Pedido(null, LocalDateTime.parse("30/09/2017 10:32", formatter), cl1, e1);
		Pedido pd2 = new Pedido(null, LocalDateTime.parse("10/10/2017 19:35", formatter), cl2, e1);

		//Pagamento pg1 = new PagamentoComCartao(null, EstadoPagamento.QUITADO, pd1, 6);
		//pd1.setPagamento(pg1);

		//Pagamento pg2 = new PagamentoComBoleto(null, EstadoPagamento.PENDENTE, pd2,
		//		LocalDateTime.parse("20/10/2017 00:00", formatter), null);
		//pd2.setPagamento(pg2);

		cl1.getPedidos().addAll(Arrays.asList(pd1));
		cl2.getPedidos().addAll(Arrays.asList(pd2));

		pedRepo.saveAll(Arrays.asList(pd1, pd2));
		//pagRepo.saveAll(Arrays.asList(pg1, pg2));

		// ITENS DE PEDIDO
		
//		ItemPedido ip1 = new ItemPedido(pd1, p1, new BigDecimal("10"), 2, p1.getPreco().multiply(BigDecimal.valueOf(2)));
//		ItemPedido ip2 = new ItemPedido(pd2, p2, new BigDecimal("10"), 2, p2.getPreco().multiply(BigDecimal.valueOf(2)));
		
	//	pd1.getItens().addAll(Arrays.asList(ip1));
	//	pd2.getItens().addAll(Arrays.asList(ip2));
		
	//	p1.getItens().addAll(Arrays.asList(ip1));
	//	p2.getItens().addAll(Arrays.asList(ip2));
		//itemRepo.saveAll(Arrays.asList(ip1, ip2));
	}
	
}
